<?php
/**
 * Basic Settings
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
define('DEBUG', 1);
set_exception_handler('myExceptionHandler');

/**
 * Should only ever constructs one PDO object, which can be used everywhere
 * @param bool $force
 * @return null|PDO
 */
function getDefaultConnection($force = false)
{
    $default = null;
    if (is_null($default) || $force == true) {
        $default = new PDO('mysql:host=localhost;dbname=currency_convert;charset=utf8', 'root', 'moot', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }
    return $default;
}

/**
 * Simple Exception Handler
 * Deals with ALL exceptions
 * @param Exception $e
 */
function myExceptionHandler(Exception $e)
{
    $exceptionDetails = [];
    $exceptionDetails['message'] = $e->getMessage();
    $exceptionDetails['code'] = $e->getCode();
    $exceptionDetails['file'] = $e->getFile();
    $exceptionDetails['line'] = $e->getLine();
    $exceptionDetails['trace'] = $e->getTraceAsString();

    if (DEBUG == 1) {
        echo '<h2>AAAAGGGGGHHHHHH YOUR END USER JUST SAW THIS BECAUSE YOU DIDN\'T CHANGE DEBUG TO 0!!!</h2>';
        echo '<p><strong>Look! details don\'t want them to see! Up in their grill!</strong></p>';
        echo '<table><tr><th>Detail Type</th><th>Detail</th></tr>';
        foreach ($exceptionDetails as $key => $value) {
            echo '<tr><th>' . htmlentities($key) . '</th><td>' . htmlentities($value) . '</td></tr>';
        }
        echo '</table>';
        exit();
    } else {
        $exceptionDetailsForFile = "==== Exception on " . time() . " ==== \n";
        foreach ($exceptionDetails as $key => $value) {
            $exceptionDetailsForFile .= $key . "\t" . $value . "\n";
        }
        $exceptionDetailsForFile .= "====  END OF EXCEPTION ==== \n\n\n";
        file_put_contents('some-file-full-of-exceptions.txt', $exceptionDetailsForFile, FILE_APPEND);
        echo '<h2>Friendly "We broke something, whoopsy" message here</h2>';
        //maybe send an email to someone or something...
        exit();
    }
}