CREATE DATABASE `currency_convert`;
USE `currency_convert`;
-- Dumping structure for table currency_convert.currency
CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UniqueCurrencyCode` (`currency_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping structure for table currency_convert.rate
CREATE TABLE IF NOT EXISTS `rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `currency_id` int(10) unsigned NOT NULL,
  `rate` float unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`date`,`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

