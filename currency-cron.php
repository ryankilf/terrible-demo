<?php
require_once('setup.php');

class InsertAllCurrencies
{
    private $_currencies = [];
    private static $insertCurrencyStmt;
    private static $getCurrencyStmt;
    private static $insertRateStmt;

    function __construct()
    {
        /**
         * Clear out the database every time
         * - N.B. This is just really for testing.
         */
        $connection = getDefaultConnection();
        $connection->query('TRUNCATE currency');
        $connection->query('TRUNCATE rate');

        $allFiles = scandir(__DIR__ . '/results');
        array_shift($allFiles);
        array_shift($allFiles);
        array_pop($allFiles);

        do {
            $fileName = __DIR__ . '/results/' . array_pop($allFiles);
            $dailyJson = json_decode(file_get_contents($fileName), true);
            $this->addRatesForOneDay($dailyJson);
        } while (count($allFiles > 0));
    }

    /**
     * Takes the json from one file, representing one date
     * @param $dailyJson
     * @throws Exception
     */
    function addRatesForOneDay($dailyJson)
    {
        if (is_null($dailyJson)) {
            throw new Exception('Daily Rates is null!!!');
        }
        $connection = getDefaultConnection();
        if (is_null(self::$insertCurrencyStmt)) {
            $insertCurrencyStmt = $connection->prepare('INSERT INTO currency (currency_code) VALUES (?)');
        }
        $insertCurrencyStmt->execute([$dailyJson['base']]);

        foreach ($dailyJson['rates'] as $currencyCode => $rate) {
            $insertCurrencyStmt->execute([$currencyCode]);

            if (is_null(self::$getCurrencyStmt)) {
                $getCurrencyStmt = $connection->prepare('SELECT id FROM currency WHERE currency_code = ?');
            }
            $getCurrencyStmt->execute([$currencyCode]);
            $this->_currencies[$currencyCode] = $getCurrencyStmt->fetchColumn(0);

            $this->addOneRateForOneDay($dailyJson['date'], $currencyCode, $rate);
        }
    }


    function addOneRateForOneDay($date, $currency, $rate)
    {
        $connection = getDefaultConnection();
        if (is_null(self::$insertRateStmt)) {
            $insertRateStmt = $connection->prepare('
                INSERT INTO rate
                ( `date`, currency_id, rate)
                VALUES
                ( :date, :currency_id, :rate)
                ');
        }
        $insertRateStmt->execute(
            [
                'date' => $date,
                'currency_id' => $this->_currencies[$currency],
                'rate' => $rate
            ]);
    }

}

$start = microtime(true);
new InsertAllCurrencies();
$end = microtime(true);
echo "==================== \n";
echo "Currencies and rates inserted \n";
echo 'total Time Spent ' . number_format($end - $start, 2) . " seconds \n";



