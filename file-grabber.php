<?php
echo '';
$dateRange = new DatePeriod(new DateTime('1 year ago'), new DateInterval('P1pdD'), new DateTime('now'));
foreach($dateRange as $date) {
    echo '- http://api.fixer.io/'.$date->format('Y-m-d')."\n";
    file_put_contents(
        'results/'.$date->format('Y-m-d').'.json',
        file_get_contents('http://api.fixer.io/'.$date->format('Y-m-d'))
    );
}

